from django.contrib import admin
from django.urls import path
# from . import views
from posts import views

from django.conf.urls.static import static
from django.conf import settings
import sitepages.views

urlpatterns = [
    path('admin/', admin.site.urls),
    # path(r'', views.index, name = 'index'),
    # path(r'past/', views.past, name = 'past'),
    # path(r'future/', views.future, name = 'future'),
    path(r'blog/<str:search_option>', views.blog, name = 'blog'),
    path('posts/<int:post_id>/', views.post_details, name="post_details"),
    path(r'', sitepages.views.home, name = 'home'),
    path(r'future/', sitepages.views.future, name = 'future'),
    path(r'past/', sitepages.views.past, name = 'past'),
    path(r'jobs/', sitepages.views.jobs, name = 'jobs')

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
