from django.http import HttpResponse
from django.shortcuts import render

def index(request):
	return render(request, 'index.html')

def past(request):
	return render(request, 'past.html')

def future(request):
	return render(request, 'future.html')

def blog(request):
	return render(request, 'blog.html')