from django.shortcuts import render

# Create your views here.


def home(request):
	return render(request, 'sitepages/home.html')

def past(request):
	return render(request, 'sitepages/past.html')

def future(request):
	return render(request, 'sitepages/future.html')

def jobs(request):
	return render(request, 'sitepages/jobs.html')