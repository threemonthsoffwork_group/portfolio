from django.db import models
from multiselectfield import MultiSelectField 

# Create your models here.
class Post(models.Model):
	title = models.CharField(max_length = 250)
	pub_date = models.DateTimeField()
	image = models.ImageField(upload_to = 'media/')
	body = models.TextField()

	STUDY_CHOICES = (('Python','Python'),
		('Software Development','Software Development'),
		('Data Engineering','Data Engineering'),
		('Statistics','Statistics'),
		('Data Science','Data Science'),
		('Surfing','Surfing'),
		)

	study_select = MultiSelectField(choices = STUDY_CHOICES)

	def __str__(self):
		return(self.title)

	def pub_date_pretty(self):
		return self.pub_date.strftime('%b %e %Y')

	def summary(self):
		return self.body[:100]