import pytest
from get_data import fetch_housing_data
import os

def test_file_downloaded():
	fetch_housing_data()

	HOUSING_PATH = os.path.join("datasets", "housing")
	assert os.path.isfile(os.path.join(HOUSING_PATH,'housing.tgz'))


def test_file_extracted():
	fetch_housing_data()
	
	HOUSING_PATH = os.path.join("datasets", "housing")
	assert os.path.isfile(os.path.join(HOUSING_PATH,'housing.csv'))
