from get_data import fetch_housing_data
import pandas as pd
import os
from typing import Optional 
import logging

logging.basicConfig(level=logging.DEBUG)
LOGGER = logging.getLogger()

HOUSING_PATH = os.path.join("datasets", "housing")

def load_housing_data(housing_path: Optional[str]=HOUSING_PATH):
    LOGGER.debug("Load from data to Pandas df from csv")
    csv_path = os.path.join(housing_path, "housing.csv")
    return pd.read_csv(csv_path)
