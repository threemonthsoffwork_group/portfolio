# Hands-on Machine Learning with Scikit-Learn, Keras and TensorFlow
This is a repo I am using to store exercises from the O'Reilly book 'Hands-on Machine Learning with Scikit-Learn, Keras and TensorFlow' as I complete them.

To run the code use the main.py script.