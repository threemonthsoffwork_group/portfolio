from get_data import fetch_housing_data
from load_data import load_housing_data
import logging

logging.basicConfig(level=logging.DEBUG)
LOGGER = logging.getLogger()

# fetch_housing_data()
housing = load_housing_data()
logging.info('\t'+ housing.head().to_string().replace('\n', '\n\t')) 

