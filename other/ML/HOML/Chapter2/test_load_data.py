import pytest
from load_data import load_housing_data
import pandas as pd
import os

def test_data_loaded():
	df = load_housing_data()

	assert isinstance(df, pd.DataFrame)